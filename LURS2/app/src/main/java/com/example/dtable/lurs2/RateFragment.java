package com.example.dtable.lurs2;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

public class RateFragment extends Fragment {
    private TextView rateTitle;
    private TextView item;
    private TextView location;
    private TextView category;
    private RatingBar ratingBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rate, container, false);
        rateTitle = (TextView) view.findViewById(R.id.rateTitle);
        item = (TextView) view.findViewById(R.id.item);
        location = (TextView) view.findViewById(R.id.location);
        category = (TextView) view.findViewById(R.id.category);
        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);


        return view;
    }
}
